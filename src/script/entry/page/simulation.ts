import riot from "riot"
require("../../tag/simulation")
require("../../tag/testload")
require("../../tag/notes")
import {parseUrlParam} from "../../util/UrlUtil"

window["param"] = parseUrlParam(window)

riot.mount("*")
