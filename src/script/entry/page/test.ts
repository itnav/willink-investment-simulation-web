import riot from "riot"
require("../../tag/testload")
import {parseUrlParam} from "../../util/UrlUtil"

window["param"] = parseUrlParam(window)

riot.mount("*")
