top
    h1 単体積立チャート
    
    .corps
        .line
        a.corp(href="./page/simulation.html?option=chart_1996042603&title=野村新興国債券投信") 野村新興国債券投信
        a.corp(href="./page/simulation.html?option=chart_1997121801") グロソブ
        a.corp(href="./page/simulation.html?option=chart_2004093002") ゼウス
        a.corp(href="./page/simulation.html?option=chart_chart_2008121101") フィデリティグロハイ
        a.corp(href="./page/simulation.html?option=chart_chart_2013071601") スマートファイブ
        a.corp(href="./page/simulation.html?option=chart_2015120701") ロボテック
        .line
    
    script.
        import riot from "riot"
        import CommonError from "../define/Error"
        
        const str: string = "aiueo"
        console.log(CommonError)
        
    style.
        @import "color.sass"
        
        top
            display: block
            margin: 16px
            & > .corps
                & > .corp
                    margin: 16px 0
                    
                & > .line
                    height: 1px
                    background: black
                    margin: 16px 0
