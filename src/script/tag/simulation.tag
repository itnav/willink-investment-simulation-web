simulation

    .fund_name
        //p simulation : {param.option}
        h1 野村新興国債券Aコース(毎月分配型)
        .line
        p 投資期間や毎月の投資金額を変更することで、過去の積立投資効果をシュミレーションできます
    
    .simulate_period
        .row
            p 投資期間 : 
            select
                option 1年
                option 3年
                option 5年
                option 10年
                option 15年
                option 20年
                option 開始月指定
                    
            p 毎月の投資金額 : 
            select
                option 1,000円
                option 5,000円
                option 10,000円
                option 30,000円
                option 50,000円
                option 金額指定
    
    script.
        import riot from "riot"
        this.param = window["param"]
        
    style.
        @import "color.sass"
        simulation
            display: block
            margin: 16px 8px
            & > .fund_name
                & > .line
                    height: 5px
                    background: #2583BA
            & > .simulate_period
                .row
                    color: #0E85F4
                    display: flex
                    flex: 1
