export function parseUrlParam(window: Window): any {
    var arg = new Object;
    var pair=window.location.search.substring(1).split('&');
    for(var i=0;pair[i];i++) {
        var kv = pair[i].split('=');
        arg[kv[0]]=kv[1];
    }
    return arg
}
